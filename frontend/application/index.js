import makeDateInputs from './modules/common_modules/date_input';
import validateForm from './modules/common_modules/form_validator';

window.addEventListener('DOMContentLoaded', function () {
    makeDateInputs();
    validateForm();
});
