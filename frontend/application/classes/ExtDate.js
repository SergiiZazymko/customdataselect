class ExtDate extends Date {
    /**
     *
     * @param year
     * @param month
     * @param day
     */
    constructor(year, month, day) {
        if (year && month && day) {
            super(year, month, day);
        } else {
            super();
        }
        /**
         *
         * @type {string[]}
         */
        this.month = [
            'Січень',
            'Лютий',
            'Березень',
            'Квітень',
            'Травень',
            'Червень',
            'Липень',
            'Серпень',
            'Вересень',
            'Жовтень',
            'Листопад',
            'Грудень'
        ];

        /**
         *
         * @type {string}
         */
        this.calendarPageHeader = `
            <tr>
                <td class="calendar_elem">НД</td>
                <td class="calendar_elem">ПН</td>
                <td class="calendar_elem">ВТ</td>
                <td class="calendar_elem">СР</td>
                <td class="calendar_elem">ЧТ</td>
                <td class="calendar_elem">ПТ</td>
                <td class="calendar_elem">СБ</td>
            </tr>   
        `;

        this.getDateString = this.getDateString.bind(this);
        this.getDaysInMonth = this.getDaysInMonth.bind(this);
        this.getWeekDay = this.getWeekDay.bind(this);
        this.getUkrMonth = this.getUkrMonth.bind(this);
    }

    /**
     *
     * @return {string}
     */
    getDateString() {
        let dd = this.getDate();
        let mm = this.getMonth() + 1;

        return [
            (dd > 9 ? '' : '0') + dd,
            (mm > 9 ? '' : '0') + mm,
            this.getFullYear(),
        ].join('/');
    };

    /**
     *
     * @return {number}
     */
    getDaysInMonth() {
        return new Date(this.getFullYear(), this.getMonth() + 1, 0).getDate();
    };

    /**
     *
     * @param dd
     * @return {number}
     */
    getWeekDay(dd) {
        return new Date(this.getFullYear(), this.getMonth(), dd).getDay();
    };

    /**
     *
     * @return {string}
     */
    getUkrMonth() {
        return this.month[this.getMonth()];
    }

}

export default ExtDate;
