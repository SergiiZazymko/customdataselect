import ExtDate from './../../classes/ExtDate';

let date = new ExtDate();

/**
 *
 * @param dateInput
 */
const makeDateInput = (dateInput) => {
    dateInput.value = date.getDateString();
    return renderCalendar(dateInput);
};

/**
 *
 * @param dateInput
 * @return {HTMLElement}
 */
const renderCalendar = (dateInput) => {
    dateInput.classList.add('calendar_elem');
    dateInput.nextElementSibling.innerHTML = null;

    let calendar = document.createElement('div');
    calendar.classList.add('calendar');
    calendar.classList.add('hidden');

    let header = document.createElement('div');
    let headerDiv = document.createElement('div');
    headerDiv.classList.add('calendar_title');
    header.classList.add('calendar_elem');
    header.innerHTML = `${date.getUkrMonth()} ${date.getFullYear()}`;
    headerDiv.append(header);

    calendar.appendChild(headerDiv);

    let calendarDiv = document.createElement('div');
    calendarDiv.classList.add('calendar_block');

    let prevButton = document.createElement('button');
    prevButton.innerHTML = '<';
    prevButton.addEventListener('click', () => {
        date = new ExtDate(
            date.getFullYear(),
            date.getMonth() - 1,
            date.getDate() < 29 ? date.getDate() : 28
        );

        let calendar = makeDateInput(dateInput);
        calendar.classList.remove('hidden');
    });
    prevButton.classList.add('calendar_elem');

    calendarDiv.appendChild(prevButton);

    let calendarPageTable = document.createElement('table');
    calendarPageTable.classList.add('calendar_table');

    calendar.addEventListener('click', function (e) {
        if (e.target.classList.contains('date')) {
            this.classList.add('hidden');
            let dd = (e.target.innerHTML > 9 ? '' : '0') + e.target.innerHTML;
            let mm = (date.getMonth() + 1 > 9 ? '' : '0') + (date.getMonth() + 1);
            let yyyy = date.getFullYear();

            dateInput.value = `${dd}/${mm}/${yyyy}`;
        }
    });

    let calendarPageTableContent = date.calendarPageHeader;

    let daysNumber = date.getDaysInMonth();

    calendarPageTableContent += '<tr>';
    calendarPageTableContent += '<td class="empty_date">&nbsp;</td>'.repeat(date.getWeekDay(1));

    for (let i = 1; i <= daysNumber; i++) {

        switch (date.getWeekDay(i)) {
            case 6:
                calendarPageTableContent += `<td class="date">${i}</td></tr>`;
                break;
            case 0:
                calendarPageTableContent += `<tr><td class="date">${i}</td>`;
                break;
            default:
                calendarPageTableContent += `<td class="date">${i}</td>`;
        }
    }
    calendarPageTableContent += '</tr>';
    calendarPageTable.innerHTML = calendarPageTableContent;

    calendarDiv.appendChild(calendarPageTable);

    let nextButton = document.createElement('button');
    nextButton.innerHTML = '>';
    nextButton.addEventListener('click', () => {
        date = new ExtDate(
            date.getFullYear(),
            date.getMonth() + 1,
            date.getDate() < 29 ? date.getDate() : 28
        );

        let calendar = makeDateInput(dateInput);
        calendar.classList.remove('hidden');

    });
    nextButton.classList.add('calendar_elem');

    calendarDiv.appendChild(nextButton);

    calendar.appendChild(calendarDiv);

    dateInput.nextElementSibling.appendChild(calendar);
    dateInput.addEventListener('click', () => {
        calendar.classList.remove('hidden');
    });

    return calendar;
};

/**
 *
 * @param e
 */
const hideCalendar = (e) => {
    if (!e.target.classList.contains('date') &&
        !e.target.classList.contains('empty_date') &&
        !e.target.classList.contains('calendar') &&
        !e.target.classList.contains('calendar_title') &&
        !e.target.classList.contains('calendar_table') &&
        !e.target.classList.contains('calendar_block') &&
        !e.target.classList.contains('calendar_elem')) {
        document.querySelectorAll('.calendar').forEach((calendar) => {
            calendar.classList.add('hidden');
        });
    }
};

/**
 *
 */
const makeDateInputs = () => {
    document.querySelectorAll('input[data-type=date]').forEach(makeDateInput);
    document.body.addEventListener('click', hideCalendar);
};

export default makeDateInputs;
