/**
 *
 */
function validateForm() {
    document.querySelector('form').addEventListener('submit', function(e) {
        let errors = [];

        let elements = Array.from(this.elements);

        elements.forEach((elem) => {
            if ('text' === elem.type) {
                if ('' === elem.value) {
                    errors.push(`Поле ${elem.name} обов'язкове для заповнення і не може бути порожнім`)
                }
            }
        });

        if (errors.length) {
            e.preventDefault();

            let errorsDiv = document.getElementById('errors');
            errorsDiv.innerHTML = null;
            errors.forEach(function (error) {

                let p = document.createElement('p');
                p.innerText = error;

                errorsDiv.appendChild(p);
            })
        }
    });
}

export default validateForm;
