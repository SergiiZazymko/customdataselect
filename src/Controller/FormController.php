<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class FormController
 * @package App\Controller
 */
class FormController extends AbstractController
{
    /**
     * @Route("/", name="form")
     * @return Response
     */
    public function index()
    {
        /** @var Request $request */
        $request = Request::createFromGlobals();

        if ('POST' == $request->getMethod()) {
            /** @var string $name */
            $name = $request->request->get('name');
            /** @var string $email */
            $email = $request->request->get('email');
            /** @var string $email */
            $birthday = $request->request->get('birthday');
            /** @var string $email */
            $university = $request->request->get('university');
            /** @var string $email */
            $universityDate = $request->request->get('universityDate');
            /** @var string $email */
            $school = $request->request->get('school');
            /** @var string $email */
            $schoolDate = $request->request->get('schoolDate');

            return $this->render('form/submitted.html.twig', [
                'name' => $name,
                'email' => $email,
                'birthday' => $birthday,
                'university' => $university,
                'universityDate' => $universityDate,
                'school' => $school,
                'schoolDate' => $schoolDate,
            ]);
        }
        return $this->render('form/form.html.twig');
    }
}
